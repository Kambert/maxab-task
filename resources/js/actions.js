import { postSchedule } from './services';

export const REQUEST_SCHEDULE_PENDING = 'REQUEST_SCHEDULE_PENDING';
export const REQUEST_SCHEDULE_SUCCEEDED = 'REQUEST_SCHEDULE_SUCCEEDED';
export const REQUEST_SCHEDULE_FAILED = 'REQUEST_SCHEDULE_FAILED';

// ACTION GENERATORS
const requestSchedulePending = () => ({
    type: REQUEST_SCHEDULE_PENDING
});

const requestScheduleSucceeded = payload => ({
    type: REQUEST_SCHEDULE_SUCCEEDED,
    payload: payload
});

const requestScheduleFailed = payload => ({
    type: REQUEST_SCHEDULE_FAILED,
    payload: payload
});

const requestSchedule = (start_date, days, number_of_sessions) => (dispatch) => {
    dispatch(requestSchedulePending());
    postSchedule(start_date, days, number_of_sessions).then((val) => {
        dispatch(requestScheduleSucceeded(val.data));
    }).catch((error) => {
        var errorMessages = [];
        for (var key in error.response.data.errors) {
            errorMessages.push(error.response.data.errors[key])
        }
        dispatch(requestScheduleFailed(errorMessages));
    });
    
};

// EXPORT ACTIONS
export { requestSchedule };
