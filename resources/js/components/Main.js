import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../reducers'
import ScheduleForm from './ScheduleForm'
import ScheduleTable from './ScheduleTable'
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import { BrowserRouter, Route, Switch, Redirect, Link } from 'react-router-dom';

const store = createStore(rootReducer, applyMiddleware(thunk, promiseMiddleware()))

export default class Main extends Component {
    render() {
        return (
            <BrowserRouter>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header">
                                    <Link to='/'>Schedule Planner</Link>
                                </div>
                                <div className="card-body">
                                    <Fragment>
                                        <Switch>
                                            <Route path='/' component={ScheduleForm} exact/>
                                            <Route path='/table' component={ScheduleTable}/>
                                            <Redirect to='/' />
                                        </Switch>
                                    </Fragment>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

if (document.getElementById('main')) {
    ReactDOM.render(
        <Provider store={store}>
            <Main />
        </Provider>,
        document.getElementById('main')
    );
}
