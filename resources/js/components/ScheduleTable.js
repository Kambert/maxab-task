import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class ScheduleTable extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                {
                    this.props.errors
                }
                <table className="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date</th>
                        <th scope="col">Chapter</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        {
                            !this.props.loading && this.props.schedule.map((row, i) =>
                            <tr key={i}>
                                    <th scope="row">{row.session_number}</th>
                                    <td>{row.date}</td>
                                    <td>{row.chapter}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}


ScheduleTable.propTypes = {
    loading: PropTypes.bool,
    schedule: PropTypes.array,
    errors: PropTypes.array
};

function mapStateToProps(state) {
    return {
        loading: state.ScheduleReducer.loading,
        schedule: state.ScheduleReducer.schedule,
        errors: state.ScheduleReducer.errors
    };
}


export default connect(
    mapStateToProps
)(ScheduleTable)
  