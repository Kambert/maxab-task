import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestSchedule } from '../actions';
import PropTypes from 'prop-types';

class ScheduleForm extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.form;
    }

    handleSubmit(e){
        e.preventDefault();
        const { start_date, day, num_of_sessions } = this.form;
        const checkboxArray = Array.prototype.slice.call(day);
        const daysValues = checkboxArray.filter(input => input.checked).map(input => input.value);
        this.props.requestSchedule(
            start_date.value,
            daysValues,
            num_of_sessions.value
        );
        this.props.history.push('/table');
    }

    render() {
        return (
            <form method="post" ref={form => this.form = form} onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="start_date">Start Date:</label>
                    <input id="start_date" type="date" name="start_date"/>
                </div>
                <div className="form-group">
                    <label htmlFor="pwd">Days</label><br/>
                    <input type="checkbox" name="day" value="0"/>Sunday<br/>
                    <input type="checkbox" name="day" value="1"/>Monday<br/>
                    <input type="checkbox" name="day" value="2"/>Tuesday<br/>
                    <input type="checkbox" name="day" value="3"/>Wednesday<br/>
                    <input type="checkbox" name="day" value="4"/>Thrusday<br/>
                    <input type="checkbox" name="day" value="5"/>Friday<br/>
                    <input type="checkbox" name="day" value="6"/>Saturday<br/>
                </div>
                <div className="form-group">
                    <label htmlFor="num_of_sessions">Days needed for chapter:</label>
                    <input id="num_of_sessions" type="number" defaultValue="1" name="num_of_sessions"/>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form> 
        );
    }
}


ScheduleForm.propTypes = {
    requestSchedule: PropTypes.func.isRequired,
    loading: PropTypes.bool,
    errors: PropTypes.array,
    history: PropTypes.object
};

function mapStateToProps(state) {
    return {
        loading: state.ScheduleReducer.loading,
        schedule: state.ScheduleReducer.schedule,
        errors: state.ScheduleReducer.errors
    };
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({ requestSchedule }, dispatch)
);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ScheduleForm)
  