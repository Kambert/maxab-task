import axios from 'axios';

export const postSchedule = (start_date, days, sessions_number) => {
    return axios.post('/api/schedule', { start_date, days, sessions_number });
};
