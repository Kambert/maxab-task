import { combineReducers } from 'redux'

import {
    REQUEST_SCHEDULE_FAILED,
    REQUEST_SCHEDULE_PENDING,
    REQUEST_SCHEDULE_SUCCEEDED
} from './actions';

const initialState = {
    loading: false,
    errors: [],
    schedule: []
};

const ScheduleReducer = (state = initialState, action) => {
    switch(action.type) {
        case REQUEST_SCHEDULE_PENDING:
            return {
                ...state,
                loading: true,
                errors: [],
                schedule: []
            };
        case REQUEST_SCHEDULE_SUCCEEDED:
            return {
                ...state,
                loading: false,
                errors: [],
                schedule: action.payload
            };
        case REQUEST_SCHEDULE_FAILED:
            return {
                ...state,
                loading: false,
                schedule: [],
                errors: action.payload
            };
        default:
            return state;
    }
};


export default combineReducers({
    ScheduleReducer
})
  