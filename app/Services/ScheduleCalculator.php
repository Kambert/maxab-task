<?php
namespace App\Services;
use Illuminate\Support\Carbon;

class ScheduleCalculator
{
    
    //  Starting date
    //  int array with number of days per week assuming the start of the week is Saturday.
    //  Example: {2,4,6}
    //  How many sessions required to finish one chapter.
    //  Example: {6}
    public static function calculate(Carbon $starting_date, array $session_days, int $sessions_for_chapter)
    {
        $number_of_chapters = 30;
        $session_needed = $sessions_for_chapter * $number_of_chapters;
        $schedule = [];
        $date = ScheduleCalculator::nextValidDate($starting_date, $session_days);
        $session_days_index = array_search($date->dayOfWeek, $session_days);
        $counter = 1;
        while($session_needed-- > 0)
        {
            array_push($schedule, [
                "session_number" => $counter,
                "date" =>  $date->toDayDateTimeString(),
                "chapter" => ceil($counter++ / $sessions_for_chapter)
            ]);
            $session_days_index = ($session_days_index + 1) % sizeof($session_days); 
            $date->next($session_days[$session_days_index]);
        }
        return $schedule;
    }

    public static function nextValidDate($date, $days)
    {
        // if the start day equal any day in the array
        if(in_array($date->dayOfWeek, $days)) 
        {
            return $date;
        }
        while(!in_array($date->add(new \DateInterval('P1D'))->dayOfWeek, $days));
        return $date;
    }
}