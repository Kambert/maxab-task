<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Services\ScheduleCalculator;

class ScheduleController extends Controller
{
    public function calculate(Request $request)
    {
        $validatedData = $request->validate([
            'days' => 'required|array',
            'sessions_number' => 'required|min:1',
            'start_date' => 'required|date',
        ]);

        $days_map = function($index) {
            if((int)$index == 0) return Carbon::SUNDAY;
            if((int)$index == 1) return Carbon::MONDAY;
            if((int)$index == 2) return Carbon::TUESDAY;
            if((int)$index == 3) return Carbon::WEDNESDAY;
            if((int)$index == 4) return Carbon::THURSDAY;
            if((int)$index == 5) return Carbon::FRIDAY;
            if((int)$index == 6) return Carbon::SATURDAY;
        };
    
        return response()->json(
            ScheduleCalculator::calculate(
                new Carbon($request->input('start_date')), 
                array_map($days_map, $request->input('days')),
                $request->input('sessions_number')
            )
        );
    }
}