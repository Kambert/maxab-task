FROM jguyomard/laravel-php:latest

MAINTAINER MK <Mohammed Kamal>

RUN apk add --update nodejs nodejs-npm

WORKDIR /var/www
COPY . /var/www
RUN composer install
RUN npm install
